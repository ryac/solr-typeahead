App = Ember.Application.create
  LOG_TRANSITIONS: true
  LOG_ACTIVE_GENERATION: true

App.ApplicationAdapter = DS.FixtureAdapter.extend()

App.ApplicationRoute = Ember.Route.extend
  actions:
    testMe: ->
      Em.Logger.info 'here!!'

# App.ApplicationController = Ember.Controller.extend {
#   firstName: 'Ryan'
#   lastName: 'Yacyshyn'
#   books: [
#     { name: 'think like da vinci'}
#     { name: 'social intelligence' }
#     { name: 'a general theory of love' }
#     { name: 'the brain that changes itself' }
#   ]
# }