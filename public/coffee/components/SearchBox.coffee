App.SearchBoxComponent = Ember.Component.extend
  tagName: 'div'

  provinces: ['british columbia', 'alberta', 'saskatchewan', 'manitoba', 'ontario', 'quebec', 'new brunswick']

  substringMatcher: (strs) ->
    (q, cb) ->
      matches = []
      substrRegex = new RegExp(q, 'i')

      $.each strs, (i, str) ->
        if (substrRegex.test(str))
          matches.push { value: str }

      cb matches

  getMovies: ->
    (q, cb) ->
      matches = []
      Em.$.get 'http://localhost:8983/solr/movies/suggest_movie', { q: q }, (res) ->
        # res.response.docs.forEach (item) ->
        #   matches.push { value: item.title }
        # cb matches
        cb res.response.docs

  actions:
    share: ->
      @sendAction('shareAction', @get('shareNetwork'))

  didInsertElement: ->
    console.log 'in..'
    $('.typeahead').typeahead { minLength: 2, hint: false, highlight: true }, { name: 'movies', source: @getMovies() }
    $('.typeahead').on 'typeahead:selected', (e) ->
      console.log 'selected..', $(e.target)[0].value