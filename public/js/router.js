App.Router.map(function() {
  return this.resource('posts');
});

App.PostsRoute = Ember.Route.extend({
  model: function() {
    return this.store.find('post');
  }
});
