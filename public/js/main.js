var App;

App = Ember.Application.create({
  LOG_TRANSITIONS: true,
  LOG_ACTIVE_GENERATION: true
});

App.ApplicationAdapter = DS.FixtureAdapter.extend();

App.ApplicationRoute = Ember.Route.extend({
  actions: {
    testMe: function() {
      return Em.Logger.info('here!!');
    }
  }
});
