App.SearchBoxComponent = Ember.Component.extend({
  tagName: 'div',
  provinces: ['british columbia', 'alberta', 'saskatchewan', 'manitoba', 'ontario', 'quebec', 'new brunswick'],
  substringMatcher: function(strs) {
    return function(q, cb) {
      var matches, substrRegex;
      matches = [];
      substrRegex = new RegExp(q, 'i');
      $.each(strs, function(i, str) {
        if (substrRegex.test(str)) {
          return matches.push({
            value: str
          });
        }
      });
      return cb(matches);
    };
  },
  getMovies: function() {
    return function(q, cb) {
      var matches;
      matches = [];
      return Em.$.get('http://localhost:8983/solr/movies/suggest_movie', {
        q: q
      }, function(res) {
        return cb(res.response.docs);
      });
    };
  },
  actions: {
    share: function() {
      return this.sendAction('shareAction', this.get('shareNetwork'));
    }
  },
  didInsertElement: function() {
    console.log('in..');
    $('.typeahead').typeahead({
      minLength: 2,
      hint: false,
      highlight: true
    }, {
      name: 'movies',
      source: this.getMovies()
    });
    return $('.typeahead').on('typeahead:selected', function(e) {
      return console.log('selected..', $(e.target)[0].value);
    });
  }
});
