Ember.TEMPLATES["application"] = Ember.Handlebars.compile("<header>\n  <nav class=\"navbar navbar-default navbar-fixed-top\" role=\"navigation\">\n    <div class=\"container-fluid\">\n      <div class=\"navbar-header\">\n        <a class=\"navbar-brand\" href=\"#\">\n          <p>Brand</p>\n        </a>\n      </div>\n      {{search-box}}\n    </div>\n  </nav>\n</header>\n<div id=\"container\">\n  {{outlet}}\n</div>");

Ember.TEMPLATES["components/search-box"] = Ember.Handlebars.compile("<form class=\"navbar-form navbar-right\" role=\"search\">\n  <div class=\"form-group the-basics\">\n    {{input value=query class=\"form-control typeahead\" placeholder=\"Search here\"}}\n  </div>\n  <button type=\"submit\" class=\"btn btn-primary\">Submit</button>\n</form>");

Ember.TEMPLATES["loading"] = Ember.Handlebars.compile("<p>LOADING...</p>");

Ember.TEMPLATES["posts"] = Ember.Handlebars.compile("aoeu\n<div class=\"row\">\n  <div class=\"columns small-12 large-6\">\n    <h3>Recent Posts yo..</h3>\n    <hr/>\n    <ul>\n      {{#each model}}\n        <li>{{title}}</li>\n      {{/each}}\n    </ul>\n    <div class=\"columns small-12 large-6\">\n      {{outlet}}\n    </div>\n  </div>\n</div>");