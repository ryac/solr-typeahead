express = require 'express'
router = express.Router()

# GET home page..
router.get '/', (req, res)->
	res.send {"response":{"numFound":4,"start":0,"docs":[{"title":"Saving Private Ryan"},{"title":"The True Story of Private Ryan"},{"title":"Into the Breach: 'Saving Private Ryan'"},{"title":"True Story of Private Ryan"}]}}

module.exports = router
