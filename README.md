Typeahead
=========

Global dependancies
-------------------

* coffee-script `npm install coffee-script -g`
* supervisor `npm install supervisor -g`

Install dependancies
--------------------
* `npm install`
* `cd public && bower install`

Start app by running `npm start`